class urlHelper {
    static getIdFromUrl(url) {
        return url.split('/')[url.split('/').length - 2];
    }
}

class ApiService {
    constructor(entity, url = 'http://swapi.dev/api') {
        this.entity = entity;
        this.url = url;
    }

    async find(filter) {
        let url = `${this.url}/${this.entity}`;
        if (filter?.page) {
            url += '?page=' + filter.page;
        }

        const {results} = await fetch(url).then(res => res.json());
        return results;
    }

    findOne(id) {
        const url = `${this.url}/${this.entity}/${id}`;
        return fetch(url).then(res => res.json());
    }
}

class Renderer {
    static renderPlanets(planets) {
        const cardsEl = document.querySelector('.js-cards');
        cardsEl.innerHTML = planets.map(planet => `
        <div class="card col-md-3" style="width: 18rem">
            <img src="https://i.pinimg.com/736x/ee/1f/11/ee1f11580848b0daf795208a9b43e56f--tatoo-star-used-equipment.jpg"
                 class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${planet.name}</h5>
                ${Renderer.getParamsContent(planet)}
                <button type="button" class="btn btn-primary js-show-more" data-bs-toggle="modal" data-planet-id="${urlHelper.getIdFromUrl(planet.url)}" data-bs-target="#staticBackdrop">
                    Узнать больше
                </button>
            </div>
        </div>`)
    }

    static renderModal(planet, films, residents) {
        const modalTitleEl = document.querySelector('.js-modal-title');
        const modalContentEl = document.querySelector('.js-modal-content');

        const planetParamsContent = Renderer.getParamsContent(planet);
        const filmsParamsContent = films.reduce((str, film) =>
            str + `<p>Episode ${film.episode_id}. ${film.title} (${film.release_date})</p>`, '');
        const getHomeworld = async url => {
            const response = await fetch(url);
            const data = await response.json();
            const homeworld = data.name;
            return homeworld;
        }

        const peopleParamsContent = residents.reduce((str, resident) =>
            str + `<p>${resident.name}: ${resident.gender}(gender), ${resident.birth_year}(birth year), ${getHomeworld(resident.homeworld)}(homeworld)</p>`, '')

        modalTitleEl.textContent = planet.name;
        modalContentEl.innerHTML = planetParamsContent + filmsParamsContent + peopleParamsContent;
    }

    static getParamsContent(planet) {
        return `
        <p class="card-text">
                <ul class="list-group">
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Диаметр:
                        <span class="dots">${planet.diameter}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Население:
                        <span class="dots">${planet.population}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Уровень гравитации:
                        <span class="dots">${planet.gravity}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Природные зоны:
                        <span class="dots">${planet.terrain}</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        Климатические зоны:
                        <span class="dots">${planet.climate}</span>
                    </li>
                </ul>
                </p>
        `
    }

    static getParams(getParams) {
        return `<p class="card-text">
                <ul class="list-group">` + getParams.map(({name, value}) => `
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        ${name}
                        <span class="badge bg-primary rounded-pill">${value}</span>
                        </li>`) + `</ul></p>`
    }
}

class Pagination {
    fn;
    page = 1;

    listen() {
        const goBackEl = document.querySelector('.js-go-back');
        const goForwardEl = document.querySelector('.js-go-forward');

        goBackEl.addEventListener('click', () => {
            this.fn(--this.page);
            if (this.fn(1)) {
                goBackEl.setAttribute('disabled', 'disabled')
            }
        });

        goForwardEl.addEventListener('click', () => {
            this.fn(++this.page);
            if (this.fn(6)) {
                goForwardEl.setAttribute('disabled', 'disabled')
            }
        });

        const go1 = document.querySelector('.js-go-1');
        go1.addEventListener('click',
            () => this.fn(1)
        );

        const go2 = document.querySelector('.js-go-2');
        go2.addEventListener('click',
            () => this.fn(2)
        );

        const go3 = document.querySelector('.js-go-3');
        go3.addEventListener('click',
            () => this.fn(3)
        );

        const go4 = document.querySelector('.js-go-4');
        go4.addEventListener('click',
            () => this.fn(4)
        );

        const go5 = document.querySelector('.js-go-5');
        go5.addEventListener('click',
            () => this.fn(5)
        );

        const go6 = document.querySelector('.js-go-6');
        go6.addEventListener('click',
            () => this.fn(6)
        );
    }

    subscribe(fn) {
        this.fn = fn;
        this.fn(this.page)
    }
}

class Modal {
    fn;

    listen() {
        const cardsEl = document.querySelector('.js-cards');
        cardsEl.addEventListener('click',
            e => {
                if (e.target.classList.contains('js-show-more')) {
                    const planetId = e.target.getAttribute('data-planet-id');
                    this.fn(planetId);
                }
            }
        )
    }

    subscribe(fn) {
        this.fn = fn;
    }
}

class App {

    constructor(
        planetService,
        peopleService,
        filmService,
        pagination,
        modalHandler
    ) {
        this.planetService = planetService;
        this.peopleService = peopleService;
        this.filmService = filmService;
        this.pagination = pagination;
        this.modalHandler = modalHandler;
    }

    bootstrap() {
        this.listen();
    }

    listen() {
        this.pagination.listen();
        this.pagination.subscribe(
            async page => {
                const planets = await this.planetService.find({page});
                Renderer.renderPlanets(planets);
            }
        );
        this.modalHandler.listen();
        this.modalHandler.subscribe(
            async planetId => {
                const planet = await this.planetService.findOne(planetId);
                const films = await Promise.all(
                    planet.films.map(filmUrl => this.filmService.findOne(
                        urlHelper.getIdFromUrl(filmUrl)))
                );
                const residents = await Promise.all(
                    planet.residents.map(peopleUrl => this.peopleService.findOne(
                        urlHelper.getIdFromUrl(peopleUrl)))
                );

                Renderer.renderModal(planet, films, residents)
            }
        );
    }
}

const planetService = new ApiService('planets');
const peopleService = new ApiService('people');
const filmService = new ApiService('films');
const pagination = new Pagination();
const modalHandler = new Modal();

const app = new App(
    planetService,
    peopleService,
    filmService,
    pagination,
    modalHandler
);

app.bootstrap();

